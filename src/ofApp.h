#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp, public ofxRunwayListener{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		//give feedback from Runway
		void runwayInfoEvent(ofJson& info);
		void runwayErrorEvent(string& message);

		//webcam -> runway1 -> runway2 -> runwayResult
		ofVideoGrabber webcam;
		ofxRunway runway1;	//PhotoSketch
		ofxRunway runway2;	//Dynamic-Style-Transfer
		ofxRunwayData data;
		ofImage runwayResult;
};
