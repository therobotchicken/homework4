#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowShape(640, 480);

	webcam.initGrabber(640, 480);

	runway1.setup(this, "http://localhost:8000");
	runway2.setup(this, "http://localhost:8001");
	runway1.start();
	runway2.start();
}

//--------------------------------------------------------------
void ofApp::update(){
	if (webcam.isInitialized())
	{
		//get webcam image
		webcam.update();
		//send webcam to runway1 and get result
		if (!runway1.isBusy())
		{
			data.setImage("input", webcam.getPixels(), OFX_RUNWAY_JPG);
			runway1.send(data);
		}
		runway1.get("output", runwayResult);
		//send runway1's output to runway2 and get result
		if (!runway2.isBusy())
		{
			data.setImage("input_image", runwayResult.getPixels(), OFX_RUNWAY_JPG);
			runway2.send(data);
		}
		runway2.get("output_image", runwayResult);
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	if (runwayResult.isAllocated())
	{
		runwayResult.draw(0, 0);
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

// Runway sends information about the current model
//--------------------------------------------------------------
void ofApp::runwayInfoEvent(ofJson& info) {
	ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message) {
	ofLogNotice("ofApp::runwayErrorEvent") << message;
}